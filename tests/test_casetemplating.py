#################################################################################
#   ParamSweep                                                                  #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import os
from .context import paramsweep, getTestTmpPath
from paramsweep.casetemplating import writeCase

tmp_test_path = getTestTmpPath()


def test_writeCase_default():
    """
    Test writing a case based on a template with default values.
    """

    caseName = "mixedEq_premix_1d_flame"
    templatePath = os.path.join(os.path.dirname(__file__), "..", "baseCases", caseName)
    destinationPath = os.path.join(tmp_test_path, caseName)

    #
    # Write Alya case with default data
    #
    writeCase(templatePath, destinationPath, parameters={}, verbose=True)

    #
    # Write mesh within it with default data
    #
    writeCase(
        os.path.join(templatePath, "mesh"),
        os.path.join(destinationPath, "mesh"),
        parameters={},
        verbose=False,
    )

    #
    # Test
    #
    assert os.path.isdir(destinationPath)

    for fe in [
        ".dat",
        ".dom.dat",
        ".ker.dat",
        ".nsi.dat",
        ".tem.dat",
        ".chm.dat",
        ".pts.dat",
    ]:
        fn = os.path.join(destinationPath, "{}{}".format(caseName, fe))
        if os.path.isfile(fn):
            #
            # Test if no template parts are left behind
            #
            with open(fn, "r") as f:
                text = f.read()
                assert not "{{" in text
                assert not "}}" in text
        else:
            #
            # Fail if there is no .dat
            #
            if fe == ".dat":
                assert False


def test_writeCase():
    """
    Test writing a case based on a template with some custom values.
    """

    caseName = "mixedEq_premix_1d_flame"
    templatePath = os.path.join(os.path.dirname(__file__), "..", "baseCases", caseName)
    destinationPath = os.path.join(tmp_test_path, caseName)

    #
    # Write Alya case with default data
    #
    writeCase(
        templatePath,
        destinationPath,
        parameters={
            "{}.dat".format(caseName): {"nstep": 666},
            "{}.ker.dat".format(caseName): {"outstep": 111},
        },
        verbose=True,
    )

    #
    # Write mesh within it with default data
    #
    writeCase(
        os.path.join(templatePath, "mesh"),
        os.path.join(destinationPath, "mesh"),
        parameters={"mesh1d.geo": {"npx": 21}},
        verbose=False,
    )

    #
    # Test
    #
    assert os.path.isdir(destinationPath)

    for fe in [
        ".dat",
        ".dom.dat",
        ".ker.dat",
        ".nsi.dat",
        ".tem.dat",
        ".chm.dat",
        ".pts.dat",
    ]:
        fn = os.path.join(destinationPath, "{}{}".format(caseName, fe))
        if os.path.isfile(fn):
            #
            # Test if no template parts are left behind
            #
            with open(fn, "r") as f:
                text = f.read()
                assert not "{{" in text
                assert not "}}" in text
        else:
            #
            # Fail if there is no .dat
            #
            if fe == ".dat":
                assert False

    #
    # Test custom content
    #
    fn = os.path.join(destinationPath, "{}{}".format(caseName, ".dat"))
    with open(fn, "r") as f:
        text = f.read()
        assert "NUMBER_OF_STEPS:        666" in text

    fn = os.path.join(destinationPath, "{}{}".format(caseName, ".ker.dat"))
    with open(fn, "r") as f:
        text = f.read()
        assert "STEPS = 111" in text

    fn = os.path.join(destinationPath, "mesh", "mesh1d.geo")
    with open(fn, "r") as f:
        text = f.read()
        assert "nx=21;" in text
