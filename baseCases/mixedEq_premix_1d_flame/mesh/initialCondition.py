import sys
import os
import math
import numpy as np

print(sys.argv)

#
# Inputs
#
S_L = {{ uinlet }}
H = {{ H }}
rhoB_per_rhoU = {{ rhoB_per_rhoU }}


#
# File names
#
meshName = sys.argv[1]

coordfile = "{}.coord".format(meshName)
velofile = "VELOC.alya"
enthfile = "ENTHA.alya"
ycfile = "YC.alya"



fCoord = open(coordfile, "r")
fVelo = open(velofile, "w")
fEnth = open(enthfile, "w")
fYc = open(ycfile, "w")

print("---| Start writing initial condition")

for line in fCoord:
    data = line.split()

    pid = int(data[0])
    dims = len(data) - 1
    x = float(data[1])
    y = float(data[2])

    
    V = 0.0
    U = S_L


    if x < 0:
        Yc = 0
    else:
        Yc = 1
        U /= rhoB_per_rhoU

    #
    # Write IC
    #
    fVelo.write("{} {} {}\n".format(pid, U, V))
    fEnth.write("{} {}\n".format(pid, H))
    fYc.write("{} {}\n".format(pid, Yc))


fCoord.close()
fVelo.close()
fEnth.close()
fYc.close()
print("---| End writing initial condition")
