#!/bin/bash
name=mesh1d
gmsh -2 -format msh2 $name.geo
gmsh2alya.pl $name -bcs=boundaries
python getCoordinates.py $name
python initialCondition.py $name
sed -i -e '1d; $d' $name.fix.bou 
