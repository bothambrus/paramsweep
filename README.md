# ParamSweep

Sweep through some parameters in Alya cases using a template case and Jinja2.

For details on installation and usage, see the [wiki](https://gitlab.com/bothambrus/paramsweep/-/wikis/home).
