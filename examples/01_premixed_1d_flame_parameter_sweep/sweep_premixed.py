#################################################################################
#   ParamSweep                                                                  #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import sys
import os
import copy
import json
import subprocess
import numpy as np
import cantera as ct

sys.path.append(os.path.join(os.path.dirname(__file__), "..", ".."))
from paramsweep.casetemplating import writeCase


#
# Define paths and parameters
#
caseName = "mixedEq_premix_1d_flame"
templatePath = os.path.join(
    os.path.dirname(__file__), "..", "..", "baseCases", caseName
)
destinationPath = "./try"
alyaparameters = {
    "{}.dat".format(caseName): {"nstep": 1000},
    "{}.ker.dat".format(caseName): {
        "zmean": 0.055,
        "zvar": 0.0,
        "cvar": 0.0,
        "outstep": 1000,
    },
    "{}.nsi.dat".format(caseName): {"uinlet": 0.1},
    "{}.tem.dat".format(caseName): {"tinlet": 298.15},
}

meshparameters = {
    "mesh1d.geo": {
        "xmin": -0.0025,
        "xmax": 0.0025,
        "npx": 101,
    },
    "initialCondition.py": {
        "uinlet": 0.1,
        "H": 1e5,
        "rhoB_per_rhoU": 0.2,
    },
}


#
# Mesh and witness locations
#
xmin = -5e-3
xmax = 5e-3
dx = 1e-3 / 30
meshparameters["mesh1d.geo"]["xmin"] = xmin
meshparameters["mesh1d.geo"]["xmax"] = xmax
meshparameters["mesh1d.geo"]["npx"] = int(np.ceil((xmax - xmin) / dx) + 1)
alyaparameters["{}.ker.dat".format(caseName)]["wit1_x"] = xmin + 5.5 * dx
alyaparameters["{}.ker.dat".format(caseName)]["wit2_x"] = xmax - 5.5 * dx


#
# Loop through cases
#
zst = 0.0551664
AFRst = 1 / zst - 1
phi = np.linspace(0.6, 1.6, 21)
# phi = np.linspace(0.6,1.6,2)
z = 1 / (1 + AFRst / phi)
zvar = [0, 0.01, 0.05, 0.1]
cvar = [0, 0.01, 0.05, 0.1]
# zvar = [0]
# cvar = [0]

gas = ct.Solution("gri30.cti")

for zloc, philoc in zip(z, phi):
    #
    # Get local parameters for Z
    #
    gas.TP = 298.15, 101325.0
    gas.set_equivalence_ratio(philoc, "CH4", "O2:0.21, N2:0.79")

    rhoU = gas.density_mass
    H = gas.enthalpy_mass

    gas.equilibrate("HP")

    rhoB = gas.density_mass

    for zvarloc in zvar:
        for cvarloc in cvar:

            #
            # Set local data
            #
            locAlyaDat = copy.deepcopy(alyaparameters)
            locMeshDat = copy.deepcopy(meshparameters)

            #
            # Parameters
            #
            locAlyaDat["{}.ker.dat".format(caseName)]["zmean"] = zloc
            locAlyaDat["{}.ker.dat".format(caseName)]["zvar"] = zvarloc
            locAlyaDat["{}.ker.dat".format(caseName)]["cvar"] = cvarloc

            locMeshDat["initialCondition.py"]["H"] = H
            locMeshDat["initialCondition.py"]["rhoB_per_rhoU"] = rhoB / rhoU

            #
            # Path
            #
            destinationPath = "./z{}_zvscal{}_cvscal{}".format(
                np.round(zloc, decimals=6),
                np.round(zvarloc, decimals=6),
                np.round(cvarloc, decimals=6),
            )

            print(
                "running case: {}, nelem={}, dx={:6.5f} mm".format(
                    destinationPath, locMeshDat["mesh1d.geo"]["npx"] - 1, dx * 1e3
                )
            )

            #
            # Write Alya case
            #
            writeCase(templatePath, destinationPath, locAlyaDat, verbose=False)

            #
            # Write mesh within it
            #
            writeCase(
                os.path.join(templatePath, "mesh"),
                os.path.join(destinationPath, "mesh"),
                locMeshDat,
                verbose=False,
            )

            #
            # Run meshing
            #
            currentDir = os.getcwd()
            os.chdir(os.path.join(destinationPath, "mesh"))

            output = subprocess.check_output(
                "./batchMesh.sh", shell=True, stderr=subprocess.STDOUT
            ).decode()
            with open("output.log", "w") as f:
                f.write(str(output))

            os.chdir(currentDir)

            #
            # Run Alya and postprocess
            #
            currentDir = os.getcwd()
            os.chdir(destinationPath)

            try:
                output = subprocess.check_output(
                    "mpirun --oversubscribe -n 6 ~/alya/dev_minor/build_Release/bin/alya {}".format(
                        caseName
                    ),
                    shell=True,
                    stderr=subprocess.STDOUT,
                    stdin=subprocess.DEVNULL
                ).decode()
            except subprocess.CalledProcessError as e:
                print("Running case failed!: {}".format(e.output.decode()))

            with open("output.log", "w") as f:
                f.write(str(output))

            ##
            ## Get vtk?
            ##
            # output = subprocess.check_output(
            #    "mpio2vtk {}".format(caseName), shell=True, stderr=subprocess.STDOUT
            # ).decode()
            # with open("post_output.log","w") as f:
            #    f.write(str(output))

            #
            # Get witness
            #
            with open("{}.chm.wit".format(caseName), "r") as f:
                line = True
                prevprevprevline = True
                prevprevline = True
                prevline = True
                while line:
                    prevprevprevline = prevprevline
                    prevprevline = prevline
                    prevline = line
                    line = f.readline()
                #
                # Get last two lines
                #
                t = float(prevprevprevline.split("=")[-1])
                YcU = float(prevprevline.split()[1])
                YcB = float(prevline.split()[1])
                SconcU = []
                SconcB = []
                for i in range(5):
                    SconcU.append(float(prevprevline.split()[9 + i]))
                    SconcB.append(float(prevline.split()[9 + i]))
                rhoU = float(prevprevline.split()[18])
                rhoB = float(prevline.split()[18])

            with open("{}.nsi.wit".format(caseName), "r") as f:
                line = True
                prevprevline = True
                prevline = True
                while line:
                    prevprevline = prevline
                    prevline = line
                    line = f.readline()
                #
                # Get last two lines
                #
                uU = float(prevprevline.split()[1])
                uB = float(prevline.split()[1])

            with open("{}.tem.wit".format(caseName), "r") as f:
                line = True
                prevprevline = True
                prevline = True
                while line:
                    prevprevline = prevline
                    prevline = line
                    line = f.readline()
                #
                # Get last two lines
                #
                TU = float(prevprevline.split()[1])
                TB = float(prevline.split()[1])

            #
            # Calculate flame speed
            #
            S_L = (uB - uU) / (rhoU / rhoB - 1)
            print(
                "S_L = {:6.3f} cm/s = ({:6.3f} - {:6.3f}) / ({:6.3f}/{:6.3f} - 1) m/s, T_in = {:6.2f} K, T_out = {:6.2f} K".format(
                    S_L * 100, uB, uU, rhoU, rhoB, TU, TB
                )
            )

            #
            # Save data
            #
            data = dict(
                t=t,
                S_L=S_L,
                YcU=YcU,
                YcB=YcB,
                SconcU=SconcU,
                SconcB=SconcB,
                rhoU=rhoU,
                rhoB=rhoB,
                uU=uU,
                uB=uB,
                TU=TU,
                TB=TB,
            )
            with open("output.json", "w") as f:
                json.dump(data, f, indent=4)

            os.chdir(currentDir)
