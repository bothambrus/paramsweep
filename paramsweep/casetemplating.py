#################################################################################
#   ParamSweep                                                                  #
#   Copyright (C) 2021  Ambrus Both                                             #
#                                                                               #
#   This program is free software: you can redistribute it and/or modify        #
#   it under the terms of the GNU General Public License as published by        #
#   the Free Software Foundation, either version 3 of the License, or           #
#   (at your option) any later version.                                         #
#                                                                               #
#   This program is distributed in the hope that it will be useful,             #
#   but WITHOUT ANY WARRANTY; without even the implied warranty of              #
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
#   GNU General Public License for more details.                                #
#                                                                               #
#   You should have received a copy of the GNU General Public License           #
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#################################################################################
import json
import os
import shutil

from jinja2 import Environment, FileSystemLoader


def writeCase(templatePath, destinationPath, parameters={}, verbose=False):
    """
    Writes Alya case from the `templatePath` to `destinationPath` by filling the template with the parameters.
    """
    #
    # Check destination
    #
    if os.path.isdir(destinationPath):
        shutil.rmtree(destinationPath)

    #
    # Copy all template path in case some files are not templates
    #
    shutil.copytree(templatePath, destinationPath)

    #
    # Get Jinja2 environment
    #
    env = Environment(
        loader=FileSystemLoader(templatePath),
    )

    #
    # Get default parameters
    #
    defdata = {}
    defjson = os.path.join(templatePath, "paramSweepDefault.json")
    if os.path.isfile(defjson):
        with open(defjson, "r") as f:
            defdata = json.load(f)

    #
    # Files to touch:
    #
    template_files = []
    for key in list(parameters.keys()) + list(defdata.keys()):
        fullpath = os.path.join(templatePath, key)
        if os.path.isfile(fullpath) and (not key in template_files):
            template_files.append(key)

    if verbose:
        print("Modifying template files from {}:".format(templatePath))
        for fn in template_files:
            print("  {}".format(fn))

    #
    # Write files based on templates
    #
    for fn in template_files:
        #
        # Get template
        #
        template = env.get_template(fn)

        #
        # Get default params
        #
        para = {}
        if fn in defdata.keys():
            for key in defdata[fn]:
                para[key] = defdata[fn][key]

        #
        # Overwrite defaults if avilable
        #
        if fn in parameters.keys():
            for key in parameters[fn]:
                para[key] = parameters[fn][key]

        #
        # Write wariables into template
        #
        with open(os.path.join(destinationPath, fn), "w") as f:
            f.write(template.render(para))
